import java.util.Scanner;

public class Loop {
    public static double Pow(double x, double n) {
        // agar daraja 0 dan kichik bo'lsa yoki manfiy bo'lsa
        if (n < 0)
            return Pow(1 / x, -1 * n);
        // agar daraja bo'lsa
        if (n == 0)
            return 1;
        // daraja 1 ga teng bo'lgan holat
        if (n == 1)
            return x;
        // daraja juft bo'lsa
        if (n % 2 == 0)
            return Pow(x * x, n / 2);
        // daraja toq bo'lsa
        return x * Pow(x * x, (n - 1) / 2);
    }

    public static void main(String[] args) {
        // int doppilar = 2;
        // // if (doppilar == 1) {
        // // System.out.println("Meing " + doppilar + " ta doppim bor.");
        // // } else {
        // // System.out.println("Meing " + doppilar + " ta doppilarim bor.");
        // // }
        // System.out.println(("Meing " + doppilar + " ta dopp") + ((doppilar == 1) ?
        // "im" : "ilarim" + " bor"));
        // Scanner scanner = new Scanner(System.in);
        // System.out.print("Farzandingiz yoshini kiritng: ");
        // int yosh = scanner.nextInt();
        // switch (yosh) {
        // case 1:
        // System.out.println("Emaklaydi");
        // break;
        // case 2:
        // System.out.println("Gapitra boshlaydi");
        // break;
        // case 3:
        // System.out.println("Yuguradi");
        // break;
        // case 4:
        // System.out.println("Yodlaydi");
        // break;
        // case 5:
        // System.out.println("Kattalar bilan gaplashadi");
        // break;
        // default:
        // System.out.println("Bu holatga tekshirmadim");
        // break;
        // }
        /*
         * // for operation
         * Scanner scanner = new Scanner(System.in);
         * System.out.print("Son kiriting: ");
         * int son = scanner.nextInt();
         * int sum = 0;
         * for (int sanoq = 0; sanoq <= son; sanoq++) {
         * sum += sanoq;
         * }
         * System.out.printf("1 dan %d gacha bo'lgan sonlar yig'indisi: %d\n", son,
         * sum);
         */
        // Scanner scanner = new Scanner(System.in);
        // System.out.print("Son kiriting: ");
        // int son = scanner.nextInt();
        // int sum = 0, count = 0;
        // while (count <= son) {
        // sum += count;
        // count++;
        // }
        // System.out.printf("1 dan %d gacha bo'lgan sonlar yig'indisi: %d\n", son,
        // sum);
        // Scanner scanner = new Scanner(System.in);
        // System.out.print("Kvadrat tomonini kiriting: ");
        // int a = scanner.nextInt();
        // int P, S;
        // P = a * 4;
        // S = a * a;
        // System.out.printf("Kvadrat perimetri: %d\nKvadrat yuzasi: %d\n", P, S);
        // Scanner scanner = new Scanner(System.in);
        // System.out.print("Parallelipiped bo'yini kiriting: ");
        // int a = scanner.nextInt();
        // System.out.print("Parallelipiped enini kiriting: ");
        // int b = scanner.nextInt();
        // System.out.print("Parallelipiped balandligini kiriting: ");
        // int c = scanner.nextInt();
        // int V, S;
        // V = a * b * c;
        // S = 2 * (a * b + b * c + a * c);
        // System.out.printf("Parallelipiped hajmi: %d\nParallelipiped to'la sirti
        // yuzasi: %d\n", V, S);

        // Scanner scanner = new Scanner(System.in);
        // int a;
        // while (true) {
        // System.out.print("Birinchi sonni kiriting: ");
        // a = scanner.nextInt();
        // if (a >= 0)
        // break;
        // else
        // continue;
        // }
        // int b;
        // while (true) {
        // System.out.print("Birinchi sonni kiriting: ");
        // b = scanner.nextInt();
        // if (b >= 0)
        // break;
        // else
        // continue;
        // }
        // double d, g;
        // d = (a + b) / 2;
        // System.out.println(Pow(4, 1 / 2));
        // int x = 1;
        // do {
        // System.out.println(x);
        // x++;
        // } while (x <= 10);
        // for (int i = 0, j = 10; i < 10 && j > 0; i++, j--) {
        // System.out.printf("%d + %d = %d\n", i, j, i + j);
        // }

        // Arraylar
        // int[][] matrix = new int[][] { { 1, 2, 3, 4, 5 }, { 5, 4, 3, 2, 1 } };
        // // for (int i = sonlar.length - 1; i >= 0; i--) {
        // // System.out.println(sonlar[i]);
        // // }
        // System.out.println(matrix[1][0]);
        // for (int i = 0; i < matrix.length; i++) {
        // for (int j = 0; j < matrix[i].length; j++) {
        // System.out.print(matrix[i][j] + " ");
        // }
        // System.out.println();
        // }
        int[][] matrix = {
                { 7, 21, 43, },
                { 65, 93, 81, 12, 70 },
                { 65, 0, 77, 19 },
                { 5, 8, 2, 7, 9, 1, 10, 5 }
        };

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}