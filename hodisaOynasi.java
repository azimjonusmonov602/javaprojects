import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JOptionPane;
public class hodisaOynasi extends JFrame{
    private JTextField matn1;  
    private JTextField matn2;
    private JTextField matn3;
    private JPasswordField parol;

    public hodisaOynasi(){
        super("Hodisalar...");
        setLayout(new FlowLayout());
        matn1 = new JTextField(10);
        add(matn1);
        matn2 = new JTextField("Menga matn yozing: ", 12);
        add(matn2);
        matn3 = new JTextField("Tahrirlashga layoqatsiz");
        matn3.setEditable(false);
        add(matn3);

        parol = new JPasswordField("Parol");
        add(parol);

        Hodisa hodisa = new Hodisa();
        matn1.addActionListener(hodisa);        
        matn2.addActionListener(hodisa);
        matn3.addActionListener(hodisa);
        parol.addActionListener(hodisa);

    }

    class Hodisa implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent hodisa){
            String xabar = "";
            if(hodisa.getSource() == matn1){
                xabar = String.format("matn1: %s", hodisa.getActionCommand());
            }
            else if(hodisa.getSource() == matn2){
                 xabar = String.format("matn2: %s", hodisa.getActionCommand());
            }
            else if(hodisa.getSource() == matn3){
                 xabar = String.format("matn3: %s", hodisa.getActionCommand());
            }
            else if(hodisa.getSource() == parol){
                 xabar = String.format("parol: %s", hodisa.getActionCommand());
            }

            JOptionPane.showMessageDialog(null, xabar);
        }
    }

}