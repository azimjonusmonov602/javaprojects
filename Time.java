public class Time{
    private int soat;   
    private int daqiqa;
    private int soniya;

    public void vaqtOrnat(int soatArg, int daqiqaArg, int soniyaArg){
        soat = (soatArg >= 0 && soatArg < 24)? soatArg:0;
        daqiqa = (daqiqaArg >=0 && daqiqaArg < 60)? daqiqaArg:0;
        soniya = (soniyaArg > 0 && soniyaArg < 60)? soniyaArg:0;
    }

    public String vaqtKorsat24(){
        return String.format("%02d:%02d:%02d", soat, daqiqa, soniya);
    }

    public String vaqtKorsat12(){
        return String.format("%d:%02d:%02d %s", (soat ==0 || soat == 12)? soat: soat % 12, daqiqa, soniya, (soat < 12)?"a.m.": "p.m.");
    }
}