import javax.swing.JOptionPane;

public class GUI{
     public static void main(String[] args) {
        String birinchiRaqam = JOptionPane.showInputDialog("Birinchi raqamni kiritng: ");
        String ikkinchiRaqam = JOptionPane.showInputDialog("ikkinchi raqamni kiritng: ");
        int raqam1 = Integer.parseInt((birinchiRaqam));
        int raqam2 = Integer.parseInt(ikkinchiRaqam);
        int yigindi = raqam1 + raqam2;
        JOptionPane.showMessageDialog(null, "Sonlarning yig'indisi: "+ yigindi, "Yig'indi", JOptionPane.INFORMATION_MESSAGE);
    }
}