import java.util.Random;

public class Massiv{
    public static void randomExample(){

        Random random = new Random();

        int massiv[] = new int[10];
        for(int i=0; i<10; i++){
            int son = random.nextInt(10) + 1;
            massiv[i] = son;
            System.out.println(massiv[i]);
        }
    }

    public static void massivExamlpe(){
        int massiv[] = {2, 5, 8, 3, 4};
        int sum = 0;
        for(int i = 0; i < massiv.length; i++){
            System.out.printf("%d   ",massiv[i]);
            sum += massiv[i];
        }
        System.out.println('\n');
        System.out.println("Yig'indi: " + sum);
    }

    public static void indexExample(int son){
        int massiv[] = {3, 2, 1, 0, 5, 4, 6, 8, 7, 9, 0};
        int index = -1, qidiruv = 8;
        for(int i=0; i < massiv.length; i ++){
            if(massiv[i] == qidiruv){
                index = i;
            }
        }
        System.out.printf("Raqam indexi %d\n", index);
    }

    public static void engKatta(){
        int massiv[] = {2, 4, 6, 9, 21, 8};
        int engKattaQiymat = massiv[0];
        int index = 0;

        for(int i = 0; i<massiv.length; i ++){
            if(engKattaQiymat<massiv[i]){
                engKattaQiymat = massiv[i];
                index = i;
            }
        }
        System.out.printf("Eng katta qiymat: %d, index: %d\n", engKattaQiymat, index);
    }

    public static void repeait(){
        int a = 4, b= 5;
        System.out.printf("a = %d, b = %d\n", a, b);
        int temp;
        temp = a;
        a = b;
        b = temp;
        System.out.printf("a = %d, b = %d\n", a, b);
    }

    public static void sort(){
        int massiv[] = {2, 0, 1};
        int temp;
        for(int i=0; i<massiv.length; i++){
            System.out.printf("%d ", massiv[i]);
        }
        System.out.println();
        for(int i=0; i<massiv.length; i++){
            for(int j = 0; j<i; j ++){
                if(massiv[i] < massiv[j]){
                    temp = massiv[i];
                    massiv[i] = massiv[j];
                    massiv[j] = temp;
                }
            }
        }
        for(int i=0; i<massiv.length; i ++)
            System.out.printf("%d ", massiv[i]);
        System.out.println();    
    }

    public static void forStrong(){
        int massiv[] = {10, 10, 10, 10};
        int yigindi = 0;
        for(int x:massiv){
            yigindi += x;
        }
        System.out.println(yigindi);
    }

    public static void ikkilikMassiv(){
        int massiv1[][]= {
            {10, 11, 12, 13}, 
            {14, 15, 16, 17},
            {18, 19, 20, 21},
        };

         int massiv2[][]= {
            {1, 2, 3, 4, 5, 6}, 
            {7, 8},
            {9, 10, 11, 12},
        };
        System.out.println("Bu birinchi massiv");
        chopet(massiv1);
        System.out.println("Bu birinchi massiv");
        chopet(massiv2);
    }

    public static void chopet(int massiv[][]){
        for(int qator = 0; qator < massiv.length; qator ++){
            for(int ustun = 0; ustun < massiv[qator].length; ustun ++){
                System.out.printf("%d  ", massiv[qator][ustun]);
            }
            System.out.println();
        }
    }
    public static double urtacha(int...raqamlar){
        double natija = 0;
        for(int x:raqamlar){
            natija += x;
        }

        return (natija/raqamlar.length);
    }
}