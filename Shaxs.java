public class Shaxs{
    private String ism;
    private Sana tugilganVaqt;

    public Shaxs(String ismi, Sana tugilganVaqti){
        ism=ismi;
        tugilganVaqt=tugilganVaqti;
    }

    public String toString(){
        return String.format("Ismi: %s, tugilgan vaqti: %s\n", ism, tugilganVaqt);
    }
}