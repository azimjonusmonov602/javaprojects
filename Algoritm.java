import java.util.Scanner;

import javax.lang.model.element.TypeElement;
import javax.sound.sampled.AudioFileFormat.Type;

public class Algoritm {

    Scanner scanner = new Scanner(System.in);

    public int shart() {
        System.out.print("Son kiriting: ");
        int a = scanner.nextInt();
        int c = 22;
        if (a > 10) {
            c = a + 3;
            return c;
        } else if (a < 10) {
            c = 2 * a;
            return c;
        } else
            return c;
    }

    public int butun() {
        System.out.print("Son kiriitng: ");
        int a = scanner.nextInt();
        int c = a / 3;
        return c;
    }

    public int qoldiq() {
        System.out.print("Son kiriitng: ");
        int a = scanner.nextInt();
        int qoldiq = a % 3;
        return qoldiq;
    }

    public String juftToq() {
        System.out.print("Son kiriitng: ");
        int a = scanner.nextInt();
        if (a % 2 == 0)
            return "juft";
        else
            return "toq";
    }

    public int kattaKichik(int a, int b) {
        if (a > b)
            return a;
        else if (a < b)
            return b;
        else
            return a;
    }

    public int engKatta(int a, int b, int c) {
        int katta = kattaKichik(a, b);
        int engKatta = kattaKichik(katta, c);
        return engKatta;
    }

    public String musbat(int a) {
        if (a > 0)
            return "musbat";
        else if (a < 0)
            return "manfiy";
        else
            return "0";
    }

    public boolean bolinish5(int a) {
        if (a % 5 == 0)
            return true;
        else
            return false;
    }

    public boolean bolinish3and4(int a) {
        if (a % 3 == 0 && a % 4 == 0)
            return true;
        else
            return false;
    }

    public boolean kabisa(int a) {
        if ((a % 4 == 0 && a % 100 != 0) || (a % 400 == 0))
            return true;
        else
            return false;
    }

    public String alpha(char c) {
        if (c >= 0 && c <= 9)
            return "number";
        else
            return "string";
    }

    public String kvadrat(int a, int b, int c){
        double diskirminant = Math.pow(b, 2) - 4 * a *c;
        if (diskirminant > 0){
                    double x1 = ((-1) * b + (Math.sqrt(diskirminant))) / 2 * a;
                    double x2 = ((-1) * b - (Math.sqrt(diskirminant))) / 2 * a;
                    return "Tenglama yechimlari: " + x1 + " " + x2;
                }
        else if (diskirminant < 0){
            return "Tengalam haqiqiy sonlar to'plamida yechimga ega emas";
        }
        else {
            double x1 = ((-1) * b) / 2 * a;
            return "Tenglama yechimi: " + x1;
        }        
    }
}