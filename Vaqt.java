public class Vaqt {
    private int soat;
    private int daqiqa;
    private int soniya;
    public Vaqt(){
        vaqtOrnat(0, 0, 0);
    }

    public Vaqt(int soatArg){
        vaqtOrnat(soatArg, 0, 0);
    }

    public Vaqt(int soatArg, int daqiqaArg){
        vaqtOrnat(soatArg, daqiqaArg, 0);
    }

    public Vaqt(int soatArg, int daqiqaArg, int soniyaArg){
        vaqtOrnat(soatArg, daqiqaArg, soniyaArg);
    }

    public void vaqtOrnat(int soatArg, int daqiqaArg, int soniyaArg){
        soatOrnat(soatArg);
        daqiqaOrnat(daqiqaArg);
        soniyaOrnat(soniyaArg);
    }

    public void soatOrnat(int soatArg){
        soat = (soatArg > 0 && soatArg < 24)? soatArg:0;
    }

    public void daqiqaOrnat(int daqiqaArg){
        daqiqa = (daqiqaArg > 0 && daqiqaArg < 60)? daqiqaArg:0;
    }

    public void soniyaOrnat(int soniyaArg){
        soniya = (soniyaArg > 0 && soniyaArg < 60)? soniyaArg:0;
    }

    public int soatKorsat(){
        return soat;
    }

     public int daqiqaKorsat(){
        return daqiqa;
    }

     public int soniyaKorsat(){
        return soniya;
    }

    public String vaqtKorsat(){
        return String.format("%02d:%02d:%02d", soatKorsat(), daqiqaKorsat(), soniyaKorsat());
    }
}