public class finalUchun{
    private int natija;
    private final int ORTTIRMA;
    public finalUchun(int orttirish){
        ORTTIRMA = orttirish;
    }

    public void orttir(){
        natija += ORTTIRMA;
    }

    public String toString(){
        return String.format("natija = %d", natija);
    }
}