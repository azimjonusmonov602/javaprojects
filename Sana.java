public class Sana{
    private int kun;
    private int oy;
    private int yil;

    public Sana(int kun, int oy, int yil) {
        this.kun = kun;
        this.oy = oy;
        this.yil = yil;
    }

    public String toString(){
        return String.format("%02d.%02d.%04d yil\n", kun, oy, yil);
    }
}